const express = require('express')
const app = express()
const testConnect = require('./db/testConnect')

class AppController {
    constructor() {
      this.express = express();
      this.middlewares();
      this.routes();
      testConnect();
    }

    middlewares() {
      this.express.use(express.json());
    }

    routes() {
      const apiRoutes= require('./route/apiRoutes')
      this.express.use('/api/v1/', apiRoutes);
    
    }
  }

  module.exports = new AppController().express;
